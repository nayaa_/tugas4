<?php 
    require_once('animal.php');
    require_once('frog.php');
    require_once('ape.php');

    $sheep = new Animal("shaun");
    echo "Nama: " . $sheep->name . "<br>"; // "shaun"
    echo "Legs: " . $sheep->legs . "<br>"; // 4
    echo "Cold blooded: " . $sheep->cold_blooded . "<br><br>"; // "no"

    $buduk = new Frog("buduk");
    echo "Nama: " . $buduk -> name . "<br>";
    echo "Legs: " . $buduk->legs . "<br>";
    echo "Cold blooded: " . $buduk->cold_blooded . "<br>"; 
    echo "Jump: " . $buduk->jump . "<br><br>";
    
    $kerasakti = new Ape("kera sakti");
    echo "Nama: " . $kerasakti -> name . "<br>";
    echo "Legs: " . $kerasakti->legs . "<br>";
    echo "Cold blooded: " . $kerasakti->cold_blooded . "<br>"; 
    echo "Yell: " . $kerasakti->yell . "<br><br>";

    
?>